Title: DebConf18 official schedule has been released, check https://debconf18.debconf.org/schedule/
Slug: 1531680649
Date: 2018-07-15 18:50
Author: Martin Zobel-Helas
Status: published

DebConf18 official schedule has been released, check [https://debconf18.debconf.org/schedule/](https://debconf18.debconf.org/schedule/)
