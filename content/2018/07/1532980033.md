Title: Thanks Debian Video Team, many videos of #DebConf18 are already available! https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/
Slug: 1532980033
Date: 2018-07-30 19:47
Author: Laura Arjona Reina
Status: published

Thanks Debian Video Team, many videos of #DebConf18 are already available! [https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/](https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/)
