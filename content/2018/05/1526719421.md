Title: MiniDebConfHambug starts today at 10h00 UTC+0200 -- watch the live streams at https:///video.debconf.org
Slug: 1526719421
Date: 2018-05-19 08:43
Author: Chris Lamb
Status: published

MiniDebConfHambug starts today at 10h00 UTC+0200 -- watch the live streams at [https:///video.debconf.org](https:///video.debconf.org)
