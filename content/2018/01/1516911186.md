Title: Preparations for (this year's) DebConf18 in Hsinchu, Taiwan are ongoing. We're looking for sponsors! https://debconf18.debconf.org/
Slug: 1516911186
Date: 2018-01-25 20:13
Author: Laura Arjona Reina
Status: published

Preparations for (this year's) DebConf18 in Hsinchu, Taiwan are ongoing. We're looking for sponsors! [https://debconf18.debconf.org/](https://debconf18.debconf.org/)
