Title: Spectre & Meltdown vulnerability/mitigation checker available in stretch-backports https://packages.debian.org/stretch-backports/spectre-meltdown-checker
Slug: 1516869600
Date: 2018-01-25 08:40
Author: Laura Arjona Reina
Status: published

Spectre & Meltdown vulnerability/mitigation checker available in stretch-backports [https://packages.debian.org/stretch-backports/spectre-meltdown-checker](https://packages.debian.org/stretch-backports/spectre-meltdown-checker)
