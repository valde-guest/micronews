Title: 25 years and counting https://bits.debian.org/2018/08/debian-is-25.html #DebianDay #Debian25years
Slug: 1534411155
Date: 2018-08-16 09:30
Author: Laura Arjona Reina
Status: published

25 years and counting [https://bits.debian.org/2018/08/debian-is-25.html](https://bits.debian.org/2018/08/debian-is-25.html) #DebianDay #Debian25years ![Debian is 25 years old by Angelo Rosa](|filename|/images/debian25years.png)
