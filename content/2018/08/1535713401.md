Title: Bits from the Debian Project Leader (August 2018) -- https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html
Slug: 1535713401
Date: 2018-08-31 11:03
Author: Chris Lamb
Status: published

Bits from the Debian Project Leader (August 2018) -- [https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html](https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html)
