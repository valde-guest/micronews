Title: There are many ways to get involved in Debian. Some of them are listed here https://www.debian.org/intro/help #DebianDay #Debian25years
Slug: 1534479966
Date: 2018-08-17 04:26
Author: Donald Norwood
Status: published

There are many ways to get involved in Debian. Some of them are listed here [https://www.debian.org/intro/help](https://www.debian.org/intro/help) #DebianDay #Debian25years
