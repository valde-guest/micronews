Title: Come join the #Debian #buster bug squashing party in Brooklyn, NYC on June 24th -- https://hashman.ca/nyc-bsp/
Slug: 1527978978
Date: 2018-06-02 22:36
Author: Chris Lamb
Status: published

Come join the #Debian #buster bug squashing party in Brooklyn, NYC on June 24th -- [https://hashman.ca/nyc-bsp/](https://hashman.ca/nyc-bsp/)
