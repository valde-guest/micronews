Title: Contribute!
Slug: contribute

# Presentation
This is a  [Debian][debian] micronews service complementing other services
such as:

 * [Debian Press Release](https://www.debian.org/News/)
 * [Debian Project News](https://www.debian.org/News/weekly/)
 * [Debian Bits, the Debian Blog](https://bits.debian.org)


Micronews is powered by [Pelican][pelican]. You can find the templates and
theme used on this blog in [our (Git) repository][gitdo]. Patches, new themes
proposals and constructive suggestions are welcome!

# How to contribute


Clone the [git](https://wiki.debian.org/git) repository. If you're a 
Debian Developer or are in the `Publicity` team in [Salsa](https://salsa.debian.org/), 
you already have write access:

    $ git clone git@salsa.debian.org:publicity-team/micronews.git


Use the `add.py` script, you need to have Python 3 installed in your system:

    $ ./add.py -a "Ana Guerrero López" -t "Debian and Tor Services available as Onion Services https://bits.debian.org/2016/08/debian-and-tor-services-available-as-onion-services.html"

Then commit and push, or simply press "y" to push immediately if you are sure.

The publicity team will see your commit and will push the micronews if there is not any issue with it. You can send a ping in `#debian-publicity` if it's urgent or it hasn't published after one or two days.

# License

This site is under the same license and copyright as the [Debian][debian]
website, see [Debian WWW pages license][wwwlicense].

# Contact us

If you want to contact us, please send an email to the [debian-publicity
mailing list][debian-publicity]. This is a publicly archived list.



[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: https://www.debian.org "Debian - The Universal Operating System"
[gitdo]: https://salsa.debian.org/publicity-team/micronews
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
