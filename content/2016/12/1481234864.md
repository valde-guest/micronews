Title: Jonas Meurer: On CVE-2016-4484, a (security)? bug in the cryptsetup initramfs integration https://blog.freesources.org//posts/2016/12/CVE-2016-4484/
Slug: 1481234864
Date: 2016-12-08 22:07
Author: Ana Guerrero López
Status: published

Jonas Meurer: On CVE-2016-4484, a (security)? bug in the cryptsetup initramfs integration [https://blog.freesources.org//posts/2016/12/CVE-2016-4484/](https://blog.freesources.org//posts/2016/12/CVE-2016-4484/)
