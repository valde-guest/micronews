Title: Next step for Debian Stretch: testing migration delay is now 10 days https://lists.debian.org/debian-devel-announce/2016/12/msg00000.html
Slug: 1481237609
Date: 2016-12-08 22:53
Author: Ana Guerrero López
Status: published

Next step for Debian Stretch: testing migration delay is now 10 days [https://lists.debian.org/debian-devel-announce/2016/12/msg00000.html](https://lists.debian.org/debian-devel-announce/2016/12/msg00000.html)
