Title: New DDs and DMs in September and October 2016. Congratulations! https://bits.debian.org/2016/11/new-developers-2016-10.html
Slug: 1478299819
Date: 2016-11-04 22:50
Author: Ana Guerrero López
Status: published

New DDs and DMs in September and October 2016. Congratulations! [https://bits.debian.org/2016/11/new-developers-2016-10.html](https://bits.debian.org/2016/11/new-developers-2016-10.html)
