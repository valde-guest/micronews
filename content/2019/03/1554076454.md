Title: The Debian Project mourns the loss of Innocent de Marchi https://www.debian.org/News/2019/20190331
Slug: 1554076454
Date: 2019-03-31 23:54
Author: Ana Guerrero López
Status: published

The Debian Project mourns the loss of Innocent de Marchi [https://www.debian.org/News/2019/20190331](https://www.debian.org/News/2019/20190331)
