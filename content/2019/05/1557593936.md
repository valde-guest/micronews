Title: New DDs and DMs in March and April 2019. Congratulations! https://bits.debian.org/2019/05/new-developers-2019-04.html
Slug: 1557593936
Date: 2019-05-11 16:58
Author: Ana Guerrero Lopez
Status: published

New DDs and DMs in March and April 2019. Congratulations! [https://bits.debian.org/2019/05/new-developers-2019-04.html](https://bits.debian.org/2019/05/new-developers-2019-04.html)
