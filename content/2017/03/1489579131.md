Title: Build Android apps with Debian: apt install android-sdk https://bits.debian.org/2017/03/build-android-apps-with-debian.html
Slug: 1489579131
Date: 2017-03-15 11:58
Author: Laura Arjona Reina
Status: published

Build Android apps with Debian: apt install android-sdk [https://bits.debian.org/2017/03/build-android-apps-with-debian.html](https://bits.debian.org/2017/03/build-android-apps-with-debian.html)
