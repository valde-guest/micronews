Title: Qt 4 removal in Debian testing (Buster)/unstable https://lists.debian.org/debian-devel-announce/2017/08/msg00006.html
Slug: 1502879398
Date: 2017-08-16 10:29
Author: Laura Arjona Reina
Status: published

Qt 4 removal in Debian testing (Buster)/unstable [https://lists.debian.org/debian-devel-announce/2017/08/msg00006.html](https://lists.debian.org/debian-devel-announce/2017/08/msg00006.html)
