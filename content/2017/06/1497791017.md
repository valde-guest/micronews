Title: To celebrate the stretch release, we have more than 45 parties in 25 different countries as well as the Debian party line https://wiki.debian.org/ReleasePartyStretch #releasingstretch
Slug: 1497791017
Date: 2017-06-18 13:03
Author: Paul Wise
Status: published

To celebrate the stretch release, we have more than 45 parties in 25 different countries as well as the Debian party line [https://wiki.debian.org/ReleasePartyStretch](https://wiki.debian.org/ReleasePartyStretch) #releasingstretch
