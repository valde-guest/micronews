Title:  #releasingstretch There are at least 15 different Debian blends to install particular parts of Debian https://www.debian.org/blends https://wiki.debian.org/DebianPureBlends
Slug: 1497751847
Date: 2017-06-18 02:10
Author: Laura Arjona Reina
Status: published

 #releasingstretch There are at least 15 different Debian blends to install particular parts of Debian [https://www.debian.org/blends](https://www.debian.org/blends) [https://wiki.debian.org/DebianPureBlends](https://wiki.debian.org/DebianPureBlends)
