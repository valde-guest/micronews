Title: "The #newinstretch game: new forensic packages in Debian/stretch" by Michael Prokop https://michael-prokop.at/blog/2017/05/25/the-newinstretch-game-new-forensic-packages-in-debianstretch/
Slug: 1495969716
Date: 2017-05-28 11:08
Author: Laura Arjona Reina
Status: published

"The #newinstretch game: new forensic packages in Debian/stretch" by Michael Prokop [https://michael-prokop.at/blog/2017/05/25/the-newinstretch-game-new-forensic-packages-in-debianstretch/](https://michael-prokop.at/blog/2017/05/25/the-newinstretch-game-new-forensic-packages-in-debianstretch/)
