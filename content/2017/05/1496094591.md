Title: Debian Policy 4.0.0.0 released https://lists.debian.org/debian-devel-announce/2017/05/msg00005.html
Slug: 1496094591
Date: 2017-05-29 21:49
Author: Laura Arjona Reina
Status: published

Debian Policy 4.0.0.0 released [https://lists.debian.org/debian-devel-announce/2017/05/msg00005.html](https://lists.debian.org/debian-devel-announce/2017/05/msg00005.html)
