Title: Debian 9.2.1 installation images released (bugfix: package ordering fixed) http://get.debian.org/images/release/9.2.1/
Slug: 1508029011
Date: 2017-10-15 00:56
Author: Laura Arjona Reina
Status: published

Debian 9.2.1 installation images released (bugfix: package ordering fixed) [http://get.debian.org/images/release/9.2.1/](http://get.debian.org/images/release/9.2.1/)
