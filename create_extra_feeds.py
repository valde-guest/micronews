#!/usr/bin/python
"""
Generates a neew feed for identi.ca without the URL in the title

2016, Ana Guerrero Lopez <ana@debian.org>
GPL v2 or any later
"""
import feedparser
import re
from feedgenerator import Atom1Feed
import sys
from time import mktime
import datetime
import pytz


def add_item_to_the_feeds(pump, facebook, item):
    # Feed generator 1.8-
    if 'published_parsed' in item:
        mdt = mktime(item['published_parsed'])
    # Feed generator 1.7 in Jessie
    if 'updated_parsed' in item:
        mdt = mktime(item['updated_parsed'])
    dt = datetime.datetime.fromtimestamp(mdt, pytz.timezone('Europe/Paris'))

    pump.add_item(
        title=re.sub('(https?://\S+)', '', item['title']),
        link=item['link'],
        unique_id=item['id'],
        description=item['summary'],
        author_name=item['author'],
        pubdate=dt)

    # Extract link for facebook: always using first link and
    # use micronews.debian.org link when there isn't any link in the text
    urls = re.findall("(?P<url>https?://[^\s]+)", item['title'])
    if len(urls) == 0:
        link = item['link']
    else:
        link = urls[0]

    facebook.add_item(
        title=re.sub('(https?://\S+)', '', item['title']),
        link=link,
        unique_id=item['id'],
        description=item['summary'],
        author_name=item['author'],
        pubdate=dt)

final_dir = sys.argv[1]
in_file = final_dir+"/feeds/atom.xml"
pump_file = final_dir+"/feeds/pump.xml"
facebook_file = final_dir+"/feeds/facebook.xml"
d = feedparser.parse(in_file)
entries = d['entries']

site_url = 'https://micronews.debian.org'
feed_domain = site_url
feed_url = 'https://micronews.debian.org/feeds/atom.xml'
feed_class = Atom1Feed

sitename = 'Debian micronews'
feed_pump = feed_class(
    title=sitename,
    link=(site_url + '/'),
    feed_url=feed_url,
    description='')

feed_facebook = feed_class(
    title=sitename,
    link=(site_url + '/'),
    feed_url=feed_url,
    description='')

for i in range(len(entries)):
    add_item_to_the_feeds(feed_pump, feed_facebook, entries[i])

with open(pump_file, 'w') as fp:
    feed_pump.write(fp, 'utf-8')

with open(facebook_file, 'w') as fp:
    feed_facebook.write(fp, 'utf-8')
